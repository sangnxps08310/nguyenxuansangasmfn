import { Component, OnInit } from '@angular/core';
import  {ModelModule} from '../../model.module';

@Component({
  selector: 'app-pop-general',
  templateUrl: './pop-general.component.html',
  styleUrls: ['./pop-general.component.css']
})
export class PopGeneralComponent implements OnInit {

  constructor(private model: ModelModule) { }

  ngOnInit() {

  }

}
