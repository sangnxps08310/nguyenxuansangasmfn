import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopGeneralComponent } from './pop-general.component';

describe('PopGeneralComponent', () => {
  let component: PopGeneralComponent;
  let fixture: ComponentFixture<PopGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
