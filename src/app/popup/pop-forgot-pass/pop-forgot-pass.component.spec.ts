import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopForgotPassComponent } from './pop-forgot-pass.component';

describe('PopForgotPassComponent', () => {
  let component: PopForgotPassComponent;
  let fixture: ComponentFixture<PopForgotPassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopForgotPassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopForgotPassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
