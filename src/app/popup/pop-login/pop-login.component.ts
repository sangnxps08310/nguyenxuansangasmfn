import {Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import {ModelModule} from '../../model.module';
import Students from '../../../assets/db/Students.json';
import {Router} from '@angular/router';

@Component({
  selector: 'app-pop-login',
  templateUrl: './pop-login.component.html',
  styleUrls: ['./pop-login.component.css']
})
export class PopLoginComponent implements OnInit {
  @ViewChild('loginModal', {static: false,}) public childModal: ElementRef;
  username: string;
  password: string;
  message: string;
  isWrong = false;

  constructor(private model: ModelModule, private router: Router) {
  }

  login() {
    const student = Students;
    if (student.find(st => this.username === st.username)) {
      if (student.find(st => this.password === st.password)) {
        this.model.setStudent(student.find(st => this.username === st.username));
        this.model.setIsLogin(true);
        this.isWrong = false;
        this.childModal.nativeElement.click();
      } else {
        this.isWrong = true;
        this.message = 'Your password is not corrected';
      }
    } else {
      this.isWrong = true;
      this.message = 'Your username is not corrected';
    }
  }

  ngOnInit() {
  }

}
