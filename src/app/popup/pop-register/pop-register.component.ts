import {Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import {ModelModule} from "../../model.module";
import student from '../../../assets/db/Students.json';

@Component({
  selector: 'app-pop-register',
  templateUrl: './pop-register.component.html',
  styleUrls: ['./pop-register.component.css']
})
export class PopRegisterComponent implements OnInit {
  @ViewChild('registerModal',{static: false,}) public childModal: ElementRef;
  student = student;
  username: '';
  password: '';
  fullname: '';
  email: '';
  gender: true;
  birthday: Date;
  confirmPass: string;

  constructor(private model: ModelModule) {
  }

  submit() {

    this.student.push({
      username: this.username,
      password: this.password,
      fullname: this.fullname,
      email: this.email,
      gender: this.gender + "",
      birthday: this.birthday.toDateString(),
      schoolfee: "5000000",
      marks: "0"
    });
    this.childModal.nativeElement.click();

  }

  ngOnInit() {
  }

}
