import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopChangePassComponent } from './pop-change-pass.component';

describe('PopChangePassComponent', () => {
  let component: PopChangePassComponent;
  let fixture: ComponentFixture<PopChangePassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopChangePassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopChangePassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
