import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QaArticleComponent } from './qa-article.component';

describe('QaArticleComponent', () => {
  let component: QaArticleComponent;
  let fixture: ComponentFixture<QaArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QaArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QaArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
