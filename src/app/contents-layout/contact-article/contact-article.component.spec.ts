import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactArticleComponent } from './contact-article.component';

describe('ContactArticleComponent', () => {
  let component: ContactArticleComponent;
  let fixture: ComponentFixture<ContactArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
