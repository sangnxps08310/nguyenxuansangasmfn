import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-contact-article',
  templateUrl: './contact-article.component.html',
  styleUrls: ['./contact-article.component.css',
    '../../../assets/vendor/bootstrap/css/bootstrap.min.css',
    '../../../assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css',
    '../../../assets/vendor/animate/animate.css',
    '../../../assets/vendor/css-hamburgers/hamburgers.min.css',
    '../../../assets/vendor/select2/select2.min.css',
    '../../../assets/css/util.css',
    '../../../assets/css/main.css']
})
export class ContactArticleComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
