import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroduceContentsGeneralComponent } from './introduce-contents-general.component';

describe('IntroduceContentsGeneralComponent', () => {
  let component: IntroduceContentsGeneralComponent;
  let fixture: ComponentFixture<IntroduceContentsGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroduceContentsGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroduceContentsGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
