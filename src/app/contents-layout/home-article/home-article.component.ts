import {Component, OnInit} from '@angular/core';
import subject from '../../../assets/db/subject.json';
import {ModelModule} from '../../model.module';
import {HttpClient} from "@angular/common/http";
// declare var require: any;
@Component({
  selector: 'app-home-article',
  templateUrl: './home-article.component.html',
  styleUrls: ['./home-article.component.css']
})
export class HomeArticleComponent implements OnInit {
  Subjects = subject;

  subjects: {
    'subjectId': 'ADAV',
    'name': 'Lập trình AndrosubjectId nâng cao',
    'logo': 'ADAV.jpg'
  }

  constructor(private model: ModelModule, private http: HttpClient) {
  }

  // public upload(fileName: string, fileContent: string): void {
  //   this.displayLoader$.next(true);
  //   this.http.put('../../../assets/db/', {name: fileName, content: fileContent})
  //     .pipe(finalize(() => this.displayLoader$.next(false)))
  //     .subscribe(res => {
  //       this.fileList.push(fileName);
  //       this.fileList$.next(this.fileList);
  //     }, error => {
  //       this.displayLoader$.next(false);
  //     });
  // }

  ofset: number = 1;
  limit: number = 4;

  getMax() {

    return this.Subjects.length / this.limit;
  }
  next() {
    // fs.writeFileSync('../../../assets/student-2.json', 'asdasd');
    // const writeFile = require('write-file');
    //
    // writeFile('../../../assets/db/qux.txt', 'some contents', function (err) {
    //   if (err) return console.log(err)
    //   console.log('file is written')
    // });
    // this.upload('test.json',JSON.stringify(this.subjects))
    if (this.ofset > this.getMax() - 1) {
      return;
    }
    this.ofset++;
  }

  pre() {
    if (this.ofset < 2) {
      return;
    }
    this.ofset--;
  }


  ngOnInit() {
  }

}
