import {Component, NgModule, OnInit} from '@angular/core';
import {RouterModule} from '@angular/router';
import {IntroduceContentsGeneralComponent} from '../introduce-contents/introduce-contents-general/introduce-contents-general.component';

@Component({
  selector: 'app-introduce-article',
  templateUrl: './introduce-article.component.html',
  styleUrls: ['./introduce-article.component.css']
})

export class IntroduceArticleComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
