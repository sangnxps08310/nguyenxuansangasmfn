import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroduceArticleComponent } from './introduce-article.component';

describe('IntroduceArticleComponent', () => {
  let component: IntroduceArticleComponent;
  let fixture: ComponentFixture<IntroduceArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroduceArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroduceArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
