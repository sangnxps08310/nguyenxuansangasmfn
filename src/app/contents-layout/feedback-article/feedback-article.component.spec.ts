import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedbackArticleComponent } from './feedback-article.component';

describe('FeedbackArticleComponent', () => {
  let component: FeedbackArticleComponent;
  let fixture: ComponentFixture<FeedbackArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedbackArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedbackArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
