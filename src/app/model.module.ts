import {Injectable} from '@angular/core';

@Injectable()
export class ModelModule {
  student: {
    username: string,
    password: string,
    fullname: string,
    email: string,
    gender: boolean,
    birthday: string,
    schoolfee: number,
    marks: number
  };
  data: string;
  isLogin: boolean = false;
  isShowModalLogin = 'none';
  isShowModalRegis = 'none';
  isShowModalChangePass = 'none';
  isShowModalFogotPass = 'none';

  setIsShowModalLogin(str: string) {
    this.isShowModalLogin = str;
  }
  setIsShowModalFogotPass(str: string) {
    this.isShowModalFogotPass = str;
  }

  setIsShowModalChangePass(str: string) {
    this.isShowModalChangePass = str;
  }
  setIsShowModalRegis(str: string) {
    this.isShowModalRegis = str;
  }
  setIsLogin(isLg: boolean) {
    this.isLogin = isLg;
  }
  getIsLogin() {
    return this.isLogin;
  }

  setStudent(st: any) {
    this.student = st;
  }

  getStudent() {
    return this.student;
  }


  setData(dt: string) {
    this.data = dt;
  }

  getData() {
    return this.data;
  }
}

