import {Component, Injectable, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import ADAV from '../../../assets/db/Quizs/ADAV.json';
import ADBS from '../../../assets/db/Quizs/ADBS.json';
import ADTE from '../../../assets/db/Quizs/ADTE.json';
import ADUI from '../../../assets/db/Quizs/ADUI.json';
import ASNE from '../../../assets/db/Quizs/ASNE.json';
import CLCO from '../../../assets/db/Quizs/CLCO.json';
import DBAV from '../../../assets/db/Quizs/DBAV.json';
import DBBS from '../../../assets/db/Quizs/DBBS.json';
import GAME from '../../../assets/db/Quizs/GAME.json';
import HTCS from '../../../assets/db/Quizs/HTCS.json';
import INMA from '../../../assets/db/Quizs/INMA.json';
import JAAV from '../../../assets/db/Quizs/JAAV.json';
import JABS from '../../../assets/db/Quizs/JABS.json';
import JSPR from '../../../assets/db/Quizs/JSPR.json';
import LAYO from '../../../assets/db/Quizs/LAYO.json';
import MOWE from '../../../assets/db/Quizs/MOWE.json';
import PHPP from '../../../assets/db/Quizs/PHPP.json';
import PMAG from '../../../assets/db/Quizs/PMAG.json';
import VBPR from '../../../assets/db/Quizs/VBPR.json';
import WEBU from '../../../assets/db/Quizs/WEBU.json';
import {ModelModule} from '../../model.module';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css'],
})
@Injectable()
export class ResultComponent implements OnInit {
  answerred: any = [];

  constructor(private route: ActivatedRoute, private model: ModelModule) {}

  detailsId = 0;
  detailsInfo: any = [];
  listDetails: Array<{ id: number, title: string, highest: number, got: number }> = [];
  show = false;
  showDetails = false;
  pass = true;
  date: Date;
  questionTotal = 0;
  scored = 0;
  percent = 0;
  questions: any = [];
  containQuestion: any = [];
  message: string;
  n: number;
  index = 0;

  getDetails(id) {
    const trueAnswer = this.containQuestion.find(cQ => id === cQ.Id);
    const yourAnswer = this.answerred.find(cQ => id === cQ.id);
    this.detailsInfo = ({answerd: yourAnswer.value, isAnswer: trueAnswer});
  }

  ngOnInit() {
    // const data = this.route.snapshot.paramMap.get('data');

    this.answerred = JSON.parse(this.model.getData());
    this.getQuestion(this.answerred[0].subject);
    for (this.n = 0; this.n < this.answerred.length; this.n++) {
      this.containQuestion.push(this.questions.find(qId => this.answerred[this.n].id === qId.Id));
    }
    this.getTotal();
  }

  getTotal() {
    for (this.n = 0; this.n < this.answerred.length; this.n++) {
      if (this.containQuestion.find(q => this.answerred[this.n].id === q.Id).AnswerId === this.answerred[this.n].value) {
        this.scored++;
        this.listDetails.push({
          id: this.answerred[this.n].id,
          title: this.containQuestion.find(q => this.answerred[this.n].id === q.Id).Text,
          highest: this.containQuestion.find(q => this.answerred[this.n].id === q.Id).Marks,
          got: this.containQuestion.find(q => this.answerred[this.n].id === q.Id).Marks
        });
      } else {
        this.listDetails.push({
          id: this.answerred[this.n].id,
          title: this.containQuestion.find(q => this.answerred[this.n].id === q.Id).Text,
          highest: this.containQuestion.find(q => this.answerred[this.n].id === q.Id).Marks,
          got: 0
        });
      }
      this.questionTotal++;
      if (this.answerred[this.n].value === 0) {
        this.questionTotal--;
      }
    }
    this.percent = (this.scored * this.answerred.length) / 100;
    this.pass = (this.scored >= 5);
  }

  getQuestion(id) {
    switch (id) {
      case 'ADAV':
        this.questions = ADAV;
        break;
      case 'ADBS':
        this.questions = ADBS;
        break;
      case 'ADTE':
        this.questions = ADTE;
        break;
      case 'ADUI':
        this.questions = ADUI;
        break;
      case 'ASNE':
        this.questions = ASNE;
        break;
      case 'CLCO':
        this.questions = CLCO;
        break;
      case 'DBAV':
        this.questions = DBAV;
        break;
      case 'DBBS':
        this.questions = DBBS;
        break;
      case 'GAME':
        this.questions = GAME;
        break;
      case 'HTCS':
        this.questions = HTCS;
        break;
      case 'INMA':
        this.questions = INMA;
        break;
      case 'JAAV':
        this.questions = JAAV;
        break;
      case 'JABS':
        this.questions = JABS;
        break;
      case 'JSPR':
        this.questions = JSPR;
        break;
      case 'LAYO':
        this.questions = LAYO;
        break;
      case 'MOWE':
        this.questions = MOWE;
        break;
      case 'PHPP':
        this.questions = PHPP;
        break;
      case 'PMAG':
        this.questions = PMAG;
        break;
      case 'VBPR':
        this.questions = VBPR;
        break;
      case 'WEBU':
        this.questions = WEBU;
        break;
    }
  }
}
