import {Component, Inject, Injectable, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import ADAV from '../../../assets/db/Quizs/ADAV.json';
import ADBS from '../../../assets/db/Quizs/ADBS.json';
import ADTE from '../../../assets/db/Quizs/ADTE.json';
import ADUI from '../../../assets/db/Quizs/ADUI.json';
import ASNE from '../../../assets/db/Quizs/ASNE.json';
import CLCO from '../../../assets/db/Quizs/CLCO.json';
import DBAV from '../../../assets/db/Quizs/DBAV.json';
import DBBS from '../../../assets/db/Quizs/DBBS.json';
import GAME from '../../../assets/db/Quizs/GAME.json';
import HTCS from '../../../assets/db/Quizs/HTCS.json';
import INMA from '../../../assets/db/Quizs/INMA.json';
import JAAV from '../../../assets/db/Quizs/JAAV.json';
import JABS from '../../../assets/db/Quizs/JABS.json';
import JSPR from '../../../assets/db/Quizs/JSPR.json';
import LAYO from '../../../assets/db/Quizs/LAYO.json';
import MOWE from '../../../assets/db/Quizs/MOWE.json';
import PHPP from '../../../assets/db/Quizs/PHPP.json';
import PMAG from '../../../assets/db/Quizs/PMAG.json';
import VBPR from '../../../assets/db/Quizs/VBPR.json';
import WEBU from '../../../assets/db/Quizs/WEBU.json';
import {Router} from '@angular/router';
import {angularMath} from 'angular-ts-math';
import {DOCUMENT} from '@angular/common';
import {ModelModule} from '../../model.module';

declare var require: any;
@Component({
  selector: 'app-quizs',
  templateUrl: './quizs.component.html',
  styleUrls: ['./quizs.component.css'],
})
@Injectable()

export class QuizsComponent implements OnInit {
  constructor(private route: ActivatedRoute, @Inject(DOCUMENT) private document: any, private router: Router, private model: ModelModule) {
  }

  index = 1;
  selected: number;
  questions: any = [];
  limit = 1;
  ofset = 1;
  id: string;
  questionIndex = [];
  n: number;
  mark = 0;
  answerredId: Array<{ subject: string, index: number, id: number, value: number }> = [];
  test: string;


  getMax() {
    const i = 0;
    return this.questionIndex.length;
  }

  getAnswerred(id: number) {
    const fs = require('fs');
    fs.writeFileSync('assets/db/tests.json', JSON.stringify(this.answerredId));

    const qId = +this.questionIndex[this.ofset - 1].Id;
    if (this.answerredId.length === 0) {
      this.answerredId.push({subject: this.id, index: this.ofset, id: qId, value: id});
    } else {
      if (this.answerredId.find(aswId => qId === aswId.id)) {
        this.answerredId.find(aswIds => qId === aswIds.id).value = id;
      } else {
        this.answerredId.push({subject: this.id, index: this.ofset, id: qId, value: id});
      }
    }
  }


  selectedAsw() {
    if (this.answerredId.find(aswIds => this.ofset === aswIds.index)) {
      return this.answerredId.find(aswIds => this.ofset === aswIds.index).value;
    }
    return 0;
  }

   record() {
    if (this.answerredId.length < 10) {
      for (this.n = 0; this.n < this.questionIndex.length; this.n++) {
        if (!this.answerredId.find(aswI => this.questionIndex[this.n].Id === aswI.id)) {
          this.answerredId.push({subject: this.id, index: 0, id: this.questionIndex[this.n].Id, value: 0});
        }
      }
    }
    this.model.setData(JSON.stringify(this.answerredId));
    this.router.navigateByUrl('/Result');
    // this.document.location.href = 'Result/' + JSON.stringify(this.answerredId);
  }

  next() {
    for (this.n = 0; this.n < 10000; this.n++) {
    }
    this.ofset += 1;
    this.questionIndex[(this.ofset - 1)];
  }

  preview() {
    for (this.n = 0; this.n < 10000; this.n++) {
    }
    this.ofset -= 1;
    this.questionIndex[this.ofset - 1];
  }


  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('subjectId');
    this.id = id;
    this.getQuestion(id);
    for (this.n = 0; this.n < 10; this.n++) {
      this.questionIndex[this.n] = this.questions[angularMath.getIntegerRandomRange(0, 50)];
    }
  }


  getQuestion(id) {
    switch (id) {
      case 'ADAV':
        this.questions = ADAV;
        break;
      case 'ADBS':
        this.questions = ADBS;
        break;
      case 'ADTE':
        this.questions = ADTE;
        break;
      case 'ADUI':
        this.questions = ADUI;
        break;
      case 'ASNE':
        this.questions = ASNE;
        break;
      case 'CLCO':
        this.questions = CLCO;
        break;
      case 'DBAV':
        this.questions = DBAV;
        break;
      case 'DBBS':
        this.questions = DBBS;
        break;
      case 'GAME':
        this.questions = GAME;
        break;
      case 'HTCS':
        this.questions = HTCS;
        break;
      case 'INMA':
        this.questions = INMA;
        break;
      case 'JAAV':
        this.questions = JAAV;
        break;
      case 'JABS':
        this.questions = JABS;
        break;
      case 'JSPR':
        this.questions = JSPR;
        break;
      case 'LAYO':
        this.questions = LAYO;
        break;
      case 'MOWE':
        this.questions = MOWE;
        break;
      case 'PHPP':
        this.questions = PHPP;
        break;
      case 'PMAG':
        this.questions = PMAG;
        break;
      case 'VBPR':
        this.questions = VBPR;
        break;
      case 'WEBU':
        this.questions = WEBU;
        break;
    }
  }
}
