import { Component, OnInit } from '@angular/core';
import {ModelModule} from "../../model.module";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(private model: ModelModule) { }

  ngOnInit() {
  }
  action() {

        this.model.setStudent(null);
        this.model.setIsLogin(false);


  }
}
