import { Component, OnInit } from '@angular/core';
import Subjects from '../../../assets/db/Subjects.json';
import {ModelModule} from "../../model.module";

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.css']
})
export class AsideComponent implements OnInit {
  Android = Subjects[0];
  Java = Subjects[1];
  Web = Subjects[2];
  Other = Subjects[3];

  constructor(private model: ModelModule) { }

  ngOnInit() {
  }

}
