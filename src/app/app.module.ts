import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './layouts/header/header.component';
import {NavComponent} from './layouts/nav/nav.component';
import {AsideComponent} from './layouts/aside/aside.component';
import {FooterComponent} from './layouts/footer/footer.component';
import {PopChangePassComponent} from './popup/pop-change-pass/pop-change-pass.component';
import {PopForgotPassComponent} from './popup/pop-forgot-pass/pop-forgot-pass.component';
import {PopLoginComponent} from './popup/pop-login/pop-login.component';
import {PopRegisterComponent} from './popup/pop-register/pop-register.component';
import {PopGeneralComponent} from './popup/pop-general/pop-general.component';
import {HomeArticleComponent} from './contents-layout/home-article/home-article.component';
import {HomeComponent} from './domain/home/home.component';
import {IntroduceComponent} from './domain/introduce/introduce.component';
import {ContactComponent} from './domain/contact/contact.component';
import {FeedbackComponent} from './domain/feedback/feedback.component';
import {QuestionAnswerComponent} from './domain/question-answer/question-answer.component';
import {IntroduceArticleComponent} from './contents-layout/introduce-article/introduce-article.component';
import {IntroduceContentsGeneralComponent} from './contents-layout/introduce-contents/introduce-contents-general/introduce-contents-general.component';
import {ContactArticleComponent} from './contents-layout/contact-article/contact-article.component';
import {FeedbackArticleComponent} from './contents-layout/feedback-article/feedback-article.component';
import {QaArticleComponent} from './contents-layout/qa-article/qa-article.component';
import {TestJsonComponent} from './test-json/test-json.component';
import {FormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import {OrderModule} from 'ngx-order-pipe';
import {QuizsComponent} from './layouts/quizs/quizs.component';
import {ResultComponent} from './layouts/result/result.component';
import {ModelModule} from './model.module';
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavComponent,
    AsideComponent,
    FooterComponent,
    PopChangePassComponent,
    PopForgotPassComponent,
    PopLoginComponent,
    PopRegisterComponent,
    PopGeneralComponent,
    HomeArticleComponent,
    HomeComponent,
    IntroduceComponent,
    ContactComponent,
    FeedbackComponent,
    QuestionAnswerComponent,
    IntroduceArticleComponent,
    IntroduceContentsGeneralComponent,
    ContactArticleComponent,
    FeedbackArticleComponent,
    QaArticleComponent,
    TestJsonComponent,
    QuizsComponent,
    ResultComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgxPaginationModule,
    OrderModule,
    HttpClientModule,
    RouterModule.forRoot([
      {
        path: '', component: HomeComponent,
        children: [
          {path: '', component: HomeArticleComponent},
          {path: 'Quiz/:subjectId', component: QuizsComponent},
          {path: 'Result', component: ResultComponent}
        ]
      },
      {path: 'introduce', component: IntroduceComponent},
      {path: 'contact', component: ContactComponent},
      {path: 'feedback', component: FeedbackComponent},
      {path: 'questionAnswer', component: QuestionAnswerComponent}
    ])
  ],
  providers: [ModelModule],
  bootstrap: [AppComponent]
})
export class AppModule { }

