import { Component, OnInit } from '@angular/core';
import Students from '../../assets/db/Subjects.json';
import person from '../../assets/db/test.json';

import { saveAs } from 'file-saver';
@Component({
  selector: 'app-test-json',
  templateUrl: './test-json.component.html',
  styleUrls: ['./test-json.component.css']
})
export class TestJsonComponent implements OnInit {
  Students: any = Students;
  person = person.person;
  test: {
    name: ''
  }
  save() {
    const blob = new Blob([JSON.stringify(this.person)], {type : 'application/json'});
    saveAs(blob, 'abc.json');

  }
  constructor() {}

  ngOnInit() {
  }

}
